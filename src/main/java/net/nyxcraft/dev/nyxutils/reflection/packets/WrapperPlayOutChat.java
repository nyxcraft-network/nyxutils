package net.nyxcraft.dev.nyxutils.reflection.packets;

import com.google.gson.Gson;
import net.nyxcraft.dev.nyxutils.reflection.CommonReflection;
import net.nyxcraft.dev.nyxutils.reflection.VersionHandler;
import org.bukkit.ChatColor;

import java.lang.reflect.Constructor;

public class WrapperPlayOutChat extends PacketWrapper {

    private static final Class<?> classPacketPlayOutChat = VersionHandler.getNMSClass("PacketPlayOutChat");
    private static final Class<?> classIChatBaseComponent = VersionHandler.getNMSClass("IChatBaseComponent");
    private static final Gson gson = (Gson) CommonReflection.getFieldValue(CommonReflection.getField(VersionHandler.getNMSClass("ChatSerializer"), "a"));
    private static final Constructor<?> constructorPacketPlayOutChat = CommonReflection.getConstructor(classPacketPlayOutChat, new Class<?>[]{ classIChatBaseComponent, byte.class });
    
    String message;
    byte b;

    public WrapperPlayOutChat(String message, byte b) {
        super(classPacketPlayOutChat);

        this.message = ChatColor.translateAlternateColorCodes('&', message);
        this.b = b;
    }
    
    public Object get() {
        return CommonReflection.constructNewInstance(constructorPacketPlayOutChat, new Object[] { getIChatBaseComponent(), b });
    }

    public Object getIChatBaseComponent() {
        return classIChatBaseComponent.cast(gson.fromJson(message, classIChatBaseComponent));
    }
}
