package net.nyxcraft.dev.nyxutils.collections;

import java.util.HashMap;

public class CaseInsensitiveHashMap<T> extends HashMap<String, T> {

    @Override
    public T put(String key, T value) {
        return super.put(key.toLowerCase(), value);
    }

    // not @Override because that would require the key parameter to be of type Object
    public T get(String key) {
        return super.get(key.toLowerCase());
    }

    public boolean containsKey(String key) {
        return super.get(key.toLowerCase()) != null;
    }

}
