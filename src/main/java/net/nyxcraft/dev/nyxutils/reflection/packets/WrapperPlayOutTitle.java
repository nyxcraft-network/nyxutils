package net.nyxcraft.dev.nyxutils.reflection.packets;

import java.lang.reflect.Constructor;

import com.google.gson.Gson;
import net.nyxcraft.dev.nyxutils.chat.TitleAction;
import net.nyxcraft.dev.nyxutils.reflection.CommonReflection;
import net.nyxcraft.dev.nyxutils.reflection.VersionHandler;

public class WrapperPlayOutTitle extends PacketWrapper {

    private static final Class<?> classEnumTitleAction = VersionHandler.getNMSClass("EnumTitleAction");
    private static final Class<?> classPacketPlayOutTitle = VersionHandler.getNMSClass("PacketPlayOutTitle");
    private static final Class<?> classIChatBaseComponent = VersionHandler.getNMSClass("IChatBaseComponent");
    private static final Gson gson = (Gson) CommonReflection.getFieldValue(CommonReflection.getField(VersionHandler.getNMSClass("ChatSerializer"), "a"));
    private static final Constructor<?> constructorPacketPlayOutTitle = CommonReflection.getConstructor(classPacketPlayOutTitle, new Class<?>[]{ classEnumTitleAction, classIChatBaseComponent, int.class, int.class, int.class });

    private TitleAction action;
    private String message;
    private int fadeIn;
    private int wait;
    private int fadeOut;

    public WrapperPlayOutTitle(TitleAction action, String message) {
        this(action, message, -1, -1, -1);
    }

    public WrapperPlayOutTitle(int fadeIn, int wait, int fadeOut) {
        this(TitleAction.TIMES, null, fadeIn, wait, fadeOut);
    }

    public WrapperPlayOutTitle(TitleAction action, String message, int fadeIn, int wait, int fadeOut) {
        super(classPacketPlayOutTitle);
        this.action = action;
        this.message = message;
        this.fadeIn = fadeIn;
        this.wait = wait;
        this.fadeOut = fadeOut;
    }
    
    public Object get() {
        return CommonReflection.constructNewInstance(constructorPacketPlayOutTitle, new Object[] { classEnumTitleAction.cast(action.getEnom()), getIChatBaseComponent(), fadeIn, wait, fadeOut });
    }

    public Object getIChatBaseComponent() {
        return classIChatBaseComponent.cast(gson.fromJson("{\"text\":\"" + message + "\"}", classIChatBaseComponent));
    }
}
