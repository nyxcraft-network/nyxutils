package net.nyxcraft.dev.nyxutils.chat;

import net.nyxcraft.dev.nyxutils.reflection.VersionHandler;

@SuppressWarnings("rawtypes")
public enum TitleAction {

    TITLE,
    SUBTITLE,
    TIMES;

    Enum enom;

    TitleAction() {
        Class<?> clazz = VersionHandler.getNMSClass("EnumTitleAction");
        Object[] constants = clazz.getEnumConstants();

        for (Object object : constants) {
            if (object instanceof Enum) {
                Enum enom = (Enum) object;
                if (enom.name().equalsIgnoreCase(this.name())) {
                    this.enom = enom;
                }
            }
        }
    }

    public Enum getEnom() {
        return enom;
    }

}
